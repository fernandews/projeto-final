import React from 'react';
import './LoginLeft.css';
import logo from './logoIN.png';
import { Link } from 'react-router-dom';

function LoginLeft() {
  return (
    <div className="loginLeft">
        <img class="logIN" src={ logo } alt="Logo"/>
        <h3>Processo Seletivo</h3>
        <p>Está aberto o processo seletivo de 2019.2
        As inscrições irão de 01/01 até 01/01.</p>
        <Link className="registerLink" to="/register">Inscreva-se</Link>
    </div>
  );
}

export default LoginLeft;