import React from 'react';
import './LoginRight.css';
import { Link } from 'react-router-dom';

function LoginRight() {
  return (
    <div className="loginRight">
        <div className="contentLoginR">
            <h3>Área do treinamento</h3>
            <input type="text" id="login" placeholder="login" />
            <input type="text" id="senha" placeholder="senha" />
            <Link to="/index-usuários">
              <button class="loginSubmit" type="submit" id="enviar">ENVIAR</button>
            </Link>
            <Link className="passwordLink" to="/passwordreset">Esqueceu sua senha?</Link>
        </div>
    </div>
  );
}

export default LoginRight;