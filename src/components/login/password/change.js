import React from 'react';
import './change.css';
import { Link } from 'react-router-dom';

function Change() {
  return (
    <div className="resetPassword">
        <div className="contentChangePSW">
            <h3>Recuperar Senha</h3>
            <input type="text" id="e-mail" placeholder="e-mail" />
            <button class="changePasswordbtn" type="submit" id="enviar">ENVIAR</button>
            <Link className="backLink" to="/">Voltar</Link>
        </div>
    </div>
  );
}

export default Change;