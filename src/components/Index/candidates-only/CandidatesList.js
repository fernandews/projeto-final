import React from 'react';
import '../UsersList.css';
import TitleIndex from '../TitleIndex/TitleIndex';
import CandidatesTable from './CandidatesTable';

function CanditatesList() {
  return (
    <div className="usersList">
        <TitleIndex />
        <CandidatesTable />
    </div>
  );
}

export default CanditatesList;