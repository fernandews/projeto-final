import React from 'react';
import axios from 'axios';
import '../Tables.css';

// const api = axios.create({
//     baseURL: '',
// });

// export default class ColabTable extends React.Component {
//     componentDidMount() {
//         api.get('/').then((response) => {
//             // adicionando o tbody à tabela
//             let table = document.querySelector('.UsersTable');
//             let tbody = document.createElement('tbody');
//             table.appendChild(tbody);

//             // criando uma tr e colocando as tds
//             let tr = document.createElement('tr');
//             let names = document.createElement('td').className('names');
//             let types = document.createElement('td').className('types');
//             let status = document.createElement('td').className('status');
//             let edit = document.createElement('td').className('edit');
//             tr.appendChild(names);
//             tr.appendChild(types);
//             tr.appendChild(status);
//             tr.appendChild(edit);

//         })
//     }
// }

function ColabTable() {
  return (
    <div className="content">
        <table className="UsersTable">
            <thead>
                <tr>
                    <th>Nome do Usuário</th>
                    <th>Tipo de Usuário</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td className="names"><p>Nominho</p></td>
                    <td className="types">Diretor</td>
                    <td className="status"></td>
                    <td className="edit"><p className="edit-btn">editar</p></td>
                </tr>
                <tr>
                    <td className="names"><p>Nominho</p></td>
                    <td className="types">Diretor</td>
                    <td className="status"></td>
                    <td className="edit"><p className="edit-btn">editar</p></td>
                </tr>
                <tr>
                    <td className="names"><p>Nominho</p></td>
                    <td className="types">Diretor</td>
                    <td className="status"></td>
                    <td className="edit"><p className="edit-btn">editar</p></td>
                </tr>
                <tr>
                    <td className="names"><p>Nominho</p></td>
                    <td className="types">Diretor</td>
                    <td className="status"></td>
                    <td className="edit"><p className="edit-btn">editar</p></td>
                </tr>
            </tbody>
        </table>
    </div>
  );
}

export default ColabTable;