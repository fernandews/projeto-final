import React from 'react';
import '../UsersList.css';
import TitleIndex from '../TitleIndex/TitleIndex';
import ColabTable from './ColabTable';

function ColabList() {
  return (
    <div className="usersList">
        <TitleIndex />
        <ColabTable />
    </div>
  );
}

export default ColabList;