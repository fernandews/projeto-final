import React from 'react';
import './TitleIndex.css';
import lupa from './lupa.png';

function TitleIndex() {
  return (
    <div className="content">
        <div className="titleIndex">
            <h3>Usuários</h3>
            <div className="search">
                <input type="text"/>
                <button type="submit"><img src={ lupa } alt="Buscar"/></button>
            </div>
        </div>
    </div>
  );
}

export default TitleIndex;