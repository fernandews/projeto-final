import React from 'react';
import '../UsersList.css';
import TitleIndex from '../TitleIndex/TitleIndex';
import AllUsersTable from './AllUsersTable';

function AllUsersList() {
  return (
    <div className="usersList">
        <TitleIndex />
        <AllUsersTable />
    </div>
  );
}

export default AllUsersList;