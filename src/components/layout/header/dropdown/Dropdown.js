import React from 'react';
import './Dropdown.css';
import menuImg from './menu.png';

function Dropdown() {
  return (
    <div class="dropdown">
		    <span><img class="menu" src={ menuImg } alt="enu"/></span>
        <ul class="dropmenu">
          <li><a href="#">Configurações</a></li>
          <li><a href="#">Log do Sistema</a></li>
          <li><a href="#">Log da Conta</a></li>
          <li><a href="#">Sair</a></li>
        </ul>
    </div>
  );
}

export default Dropdown;