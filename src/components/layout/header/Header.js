import React from 'react';
import './Header.css';
import logo from './logoIN.png';
import Dropdown from './dropdown/Dropdown';

function Header() {
  return (
    <div className="header">
      <div className="content">
        <img class="logo" src={ logo } alt="Logo"/>
        <Dropdown />
      </div>
    </div>
  );
}

export default Header;