import React from 'react';
import Header from './header/Header';
import MenuLeft from './menu/MenuLeft';

function Layout() {
  return (
    <section className="layout">
        <Header />
        <MenuLeft />
    </section>
  );
}

export default Layout;