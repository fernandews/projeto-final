import React from 'react';
import './MenuLeft.css';
import { Link } from 'react-router-dom';
import Accordion from './accordion/Accordion';

function MenuLeft() {
  return (
    <section className="menuLeft">
        <Link className="linktoDashboard" to="#">
            <div className="goToDashboard">Dashboard</div>
        </Link>
        <Accordion />           
    </section>
  );
}

export default MenuLeft;