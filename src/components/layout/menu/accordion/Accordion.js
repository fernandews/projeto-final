import React from 'react';
import './Accordion.css';
import { Link } from 'react-router-dom';

function Accordion() {
  return (
    <div className="accordion">
        <div className="accordion-item">
            <input type="checkbox" name="accordion" id="accordion-1" />
            <label for= "accordion-1">
                <div className="accordion-name">
                    <p>Processos Seletivos</p>
                    <p className="arrow">&#11167;</p>
                </div>
            </label>
            <div class="accordion-content">
                <ul>
                    <Link to="/process-seletivo"><li>Processo seletivo vigente</li></Link>    
                    <Link to="/todos-ps"><li>Todos os processos seletivos</li></Link>
                    <Link to="/criar-ps"><li>Criar processo seletivo</li></Link>
                </ul>
            </div>
            <div className="accordion-item">
            <input type="checkbox" name="accordion" id="accordion-2" />
            <label for= "accordion-2">
                <div className="accordion-name">
                    <p>Usuários</p>
                    <p className="arrow">&#11167;</p>
                </div>
            </label>
            <div class="accordion-content">
                <ul>
                    <Link to="/index-usuários"><li>Todos os usuários</li></Link>
                    <Link to="/candidatos"><li>Candidatos</li></Link>
                    <Link to="/colaboradores"><li>Colaboradores</li></Link>
                    <Link to="/criar-usuário"><li>Criar Usuário</li></Link>
                </ul>
            </div>
        </div>
        </div>
    </div>
  );
}

export default Accordion;