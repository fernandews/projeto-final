import React from 'react';
import { Link } from 'react-router-dom';
import './courses-card.css';

function CourseCards() {
    return (
        <div className="content allCards">
        <Link to="/">
            <div className="small card">
                <p>Nome do Curso</p>
                <p>10/02 - 20/01</p>
            </div>
        </Link>
        <Link to="/">
            <div className="small card">
                <p>Nome do Curso</p>
                <p>10/02 - 20/01</p>
            </div>
        </Link>
        <Link to="/">
            <div className="small card">
                <p>Nome do Curso</p>
                <p>10/02 - 20/01</p>
            </div>
        </Link>
        <Link to="/">
            <div className="small card">
                <p>Nome do Curso</p>
                <p>10/02 - 20/01</p>
            </div>
        </Link>
        <Link to="/">
            <div className="small card">
                <p>Nome do Curso</p>
                <p>10/02 - 20/01</p>
            </div>
        </Link>
    </div>
    )
}

export default CourseCards;