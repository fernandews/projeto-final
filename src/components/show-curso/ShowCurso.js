import React from 'react';
import './showCurso.css';

export default function ShowChurso () {
    return(
        <section className="content flexCurso">
            <div className="first card">
                <span className="title">
                    <h5>Nome do curso</h5>
                    <p>01/02 - 20/10</p>
                </span>
                <span className="cardContent">
                    <p className="negrito">Ementa:</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas luctus eros a sodales consectetur. Quisque sit amet lectus vel erat lobortis suscipit. Curabitur luctus efficitur ullamcorper. Donec id aliquam sapien. Duis posuere dui vel nunc finibus facilisis. Integer at erat congue, molestie sem ut, mollis nunc. Pellentesque fermentum pellentesque consequat. Etiam volutpat mollis sollicitudin. Integer ut elit non orci rhoncus consequat. Nunc lacinia lacus in egestas malesuada. Vivamus feugiat nunc sem, a porta dolor pellentesque interdum. Aliquam maximus metus consequat faucibus blandit. Ut congue risus vitae ipsum vulputate maximus.</p>
                    <p className="negrito">Link de configuração do ambiente:</p>
                    <a href="#">nadaaqui.com</a>
                </span>
            </div>
            <div className="scnd-row card">
                <p>Nome da Aula</p>
                <p>10/02 - 20/01</p>
            </div>
            <div className="scnd-row card">
                <p>Nome da Aula</p>
                <p>10/02 - 20/01</p>
            </div>
            <div className="scnd-row card">
                <p>Nome da Aula</p>
                <p>10/02 - 20/01</p>
            </div>
            <div className="first card">
                <ul>
                    <li className="negrito">Link do material:</li>
                    <li>Morbi eu sodales neque, a mollis ante. Pellentesque in lacinia est.</li>
                    <li>In hac habitasse platea dictumst. Aenean sed sem quam.</li>
                    <li>Proin erat ante, consectetur pretium lectus in, dignissim commodo purus. </li>
                </ul>
            </div> 
        </section>
    )
}