import React from 'react';
import Header from '../components/layout/header/Header';
import MenuLeft from '../components/layout/menu/MenuLeft';
import CourseCards from '../components/home-instrutor/courses-card';


function HomeInstrutor() {
  return (
    <section>
        <Header />
        <MenuLeft />
        <CourseCards />
    </section>
  );
}

export default HomeInstrutor;