import React from 'react';
import '../css/login.css';
import LoginLeft from '../components/login/esquerda/LoginLeft';
import LoginRight from '../components/login/direita/LoginRight';

function Login() {
  return (
    <section className="login">
        <LoginLeft />
        <LoginRight/>
    </section>
  );
}

export default Login;