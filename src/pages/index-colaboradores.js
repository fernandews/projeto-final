import React from 'react';
import Header from '../components/layout/header/Header';
import MenuLeft from '../components/layout/menu/MenuLeft';
import ColabList from '../components/Index/colabs/ColabList';


function IndexColabs() {
  return (
    <section>
        <Header />
        <MenuLeft />
        <ColabList />
    </section>
  );
}

export default IndexColabs;