import React from 'react';
import '../css/ChangePassword.css';
import LoginLeft from '../components/login/esquerda/LoginLeft';
import Change from '../components/login/password/change';

function ChangePassword() {
  return (
    <section className="changePassword">
        <LoginLeft />
        <Change />
    </section>
  );
}

export default ChangePassword;