import React from 'react';
import '../css/NotFound.css';
import Header from '../components/layout/header/Header';
import icon from '../img/img404.png';
import { Link } from 'react-router-dom';

function NotFound() {
  return (
    <section className="notFound">
        <Header />
        <div className="content">
          <div className="nf-txt">
            <h2>404</h2>
            <img class="icon" src={ icon }/>
            <p>Oops, essa página não existe.</p>
            <Link onClick={() => {window.history.go(-1)}} >Voltar</Link>
          </div>
        </div>
    </section>
  );
}

export default NotFound;