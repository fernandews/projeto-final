import React from 'react';
import Header from '../components/layout/header/Header';
import MenuLeft from '../components/layout/menu/MenuLeft';
import ShowCurso from '../components/show-curso/ShowCurso';


function ShowCursoPage() {
  return (
    <section>
        <Header />
        <MenuLeft />
        <ShowCurso />
    </section>
  );
}

export default ShowCursoPage;