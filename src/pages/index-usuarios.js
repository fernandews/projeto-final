import React from 'react';
import Header from '../components/layout/header/Header';
import MenuLeft from '../components/layout/menu/MenuLeft';
import AllUsersList from '../components/Index/all-users/AllUsersList';


function Main() {
  return (
    <section>
        <Header />
        <MenuLeft />
        <AllUsersList />
    </section>
  );
}

export default Main;