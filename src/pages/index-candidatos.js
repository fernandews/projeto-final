import React from 'react';
import Header from '../components/layout/header/Header';
import MenuLeft from '../components/layout/menu/MenuLeft';
import CandidatesList from '../components/Index/candidates-only/CandidatesList';


function IndexCandidates() {
  return (
    <section>
        <Header />
        <MenuLeft />
        <CandidatesList />
    </section>
  );
}

export default IndexCandidates;