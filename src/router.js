import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './pages/login';
import ChangePassword from './pages/ChangePassword';
import Main from './pages/index-usuarios';
import IndexCandidates from './pages/index-candidatos';
import IndexColabs from './pages/index-colaboradores';
import NotFound from './pages/NotFound';
import Building from './pages/building';
import HomeInstrutor from './pages/homeInstrutor';
import ShowCursoPage from './pages/ShowCurso';



export default function AppRouter() {
    return (
        <Router>
            <Switch>
                <Route exact path='/' component={Login} />
                <Route exact path='/passwordreset' component={ChangePassword} />

                <Route exact path='/process-seletivo' component={ Building } />
                <Route exact path='/todos-ps' component={ Building } />
                <Route exact path='/criar-ps' component={ Building } />

                <Route exact path='/index-usuários' component={Main} />
                <Route exact path='/candidatos' component={IndexCandidates} />
                <Route exact path='/colaboradores' component={IndexColabs} />
                <Route exact path='/criar-usuário' component={ Building } />

                <Route exact path='/home-instrutor' component={ HomeInstrutor } />
                <Route exact path='/show-curso' component={ ShowCursoPage } />

                <Route component={NotFound} />
            </Switch>
        </Router>
    )
}